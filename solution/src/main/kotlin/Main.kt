package org.bsc.katas.vendingmachine

const val NICKEL_WEIGHT = 5.0f
const val NICKEL_DIAMETER = 21.21f
const val DIME_WEIGHT = 2.268f
const val DIME_DIAMETER = 17.91f
const val QUARTER_WEIGHT = 5.670f
const val QUARTER_DIAMETER = 24.26f

class VendingMachine() {
    val coinReturnTray: MutableList<Coin> = mutableListOf()
    var display = "INSERT COIN"
    var currentAmount = 0.0f

    private fun isNickel(coin: Coin) = coin.weight == NICKEL_WEIGHT && coin.diameter == NICKEL_DIAMETER
    private fun isDime(coin: Coin) = coin.weight == DIME_WEIGHT && coin.diameter == DIME_DIAMETER
    private fun isQuarter(coin: Coin) = coin.weight == QUARTER_WEIGHT && coin.diameter == QUARTER_DIAMETER

    fun accept(coin: Coin) {
        fun isValid() = isNickel(coin) || isDime(coin) || isQuarter(coin)
        fun rejectCoin() {
            display = "COIN REJECTED"
            coinReturnTray.add(coin)
        }
        fun showCurrentValue() {
            currentAmount += getValue(coin)
            display = currentAmount.toString()
        }

        if (isValid()) showCurrentValue() else rejectCoin()
    }

    fun getValue(coin: Coin) = when {
        isNickel(coin) -> 0.05f
        isDime(coin) -> 0.10f
        isQuarter(coin) -> 0.25f
        else -> 0.00f
    }
}

class Coin(val weight: Float, val diameter: Float)
