package org.bsc.katas.vendingmachine

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class MainTest {

    @Test fun `when an invalid coin is entered verify rejection`() {
        val machine = VendingMachine()
        val coin = Coin(0f, 0f)
        machine.accept(coin)
        assertEquals("COIN REJECTED", machine.display)
        assertEquals(1, machine.coinReturnTray.size)
        assertEquals(0.0f, machine.getValue(coin))
    }

    @Test fun `when a valid coin is presented verify acceptance and value`() {
        val nickel = Coin(NICKEL_WEIGHT, NICKEL_DIAMETER)
        val dime = Coin(DIME_WEIGHT, DIME_DIAMETER)
        val quarter = Coin(QUARTER_WEIGHT, QUARTER_DIAMETER)
        val pairs = listOf(Pair(0.05f, nickel), Pair(0.10f, dime), Pair(0.25f, quarter))
        val machine = VendingMachine()
        pairs.forEach {
            machine.accept(it.second)
            assertTrue { machine.coinReturnTray.isEmpty() }
            assertEquals(it.first, machine.getValue(it.second))
        }
    }

    @Test fun `when the machine is started verify the display shows the insert coin message`() {
        val machine = VendingMachine()
        assertEquals("INSERT COIN", machine.display)
    }
}
